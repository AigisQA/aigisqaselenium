from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

class LoginPage:
    textbox_userName_id ='userName'
    textbox_password_id = 'password'
    button_login_id = 'login'
    button_Logout_id = 'logout'
    button_Logout_dropMenu_id = 'applicationDropdownMenuButton'

    def __init__(self, driver):
        self.driver = driver

    def setUserName(self, username):
        self.driver.find_element_by_id(self.textbox_userName_id).clear()
        self.driver.find_element_by_id(self.textbox_userName_id).send_keys(username)

    def password(self, password):
        self.driver.find_element_by_id(self.textbox_password_id).clear()
        self.driver.find_element_by_id(self.textbox_password_id).send_keys(password)

    def ClickLogin(self):
        self.driver.find_element_by_id(self.button_login_id).click()


    def ClickLogout(self):
        self.driver.find_element_by_id(self.button_Logout_id).click()
    def ClickLogoutMenu(self):
        self.driver.find_element(By.ID, self.button_Logout_dropMenu_id).click()