import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

from pageObjects.LoginPage import LoginPage
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen
from utilities import XLUtils
import time


class Test_002_DDT_Login:
    baseURL = ReadConfig.getApplicationURL()
    path = ".//TestData/LoginData.xlsx"

    logger = LogGen.loggen()

    @pytest.mark.regression
    def test_Login_DDT(self, setup):
        self.logger.info("**************************Test_002_DDT_Login **************************")
        self.logger.info("************************** Verifying Login Test**************************")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)

        self.row = XLUtils.getRowCount(self.path, 'Sheet1')

        for r in range(2, self.row + 1):
            self.username = XLUtils.readData(self.path, 'Sheet1', r, 1)
            self.password = XLUtils.readData(self.path, 'Sheet1', r, 2)
            self.exp  = XLUtils.readData(self.path, 'Sheet1', r, 3)

            self.lp.setUserName(self.username)
            self.lp.password(self.password)
            self.lp.ClickLogin()

            # set implicit wait time
            self.driver.implicitly_wait(30)  # seconds
            time.sleep(10)

            lst_status = []

            '''

            inputboxes = self.driver.find_elements(By.CLASS_NAME, 'PW-inputbox')



            if not inputboxes:
                if self.status == 'Pass':
                    statuslist.append("Pass")
                    self.logger.info(" **** Test is Passed ****")
                    self.lp.ClickLogoutMenu()
                    self.lp.ClickLogout()
                elif self.status == "Fail":
                    statuslist.append("Fail")
                    self.logger.info(" **** Test is Failed ****")
                    self.lp.ClickLogoutMenu()
                    self.lp.ClickLogout()
            else:
                if self.status == 'Pass':
                    statuslist.append("Fail")
                    self.logger.info(" **** Test is Failed ****")
                elif self.status == "Fail":
                    statuslist.append("Pass")
                    self.logger.info(" **** Test is Passed ****")
        print(statuslist)
        if "Fail" not in statuslist:
            self.logger.info(" **** Login DDT test is Passed****")
            self.driver.close()
            assert True
        else:
            self.logger.info(" **** Login DDT test is Failed ****")
            self.driver.close()
            assert False    '''
        #self.lp.ClickLogoutMenu()
        #self.driver.implicitly_wait(2)  # seconds
        #self.lp.ClickLogout()
            act_title = self.driver.title
            exp_title = "Aigis"

            if act_title == exp_title:
                if self.exp == 'Pass':
                    self.logger.info("**** passed ****")
                    self.lp.ClickLogoutMenu()
                    self.lp.ClickLogout();
                    lst_status.append("Pass")
                elif self.exp == 'Fail':
                    self.logger.info("**** failed ****")
                    self.lp.ClickLogoutMenu()
                    self.lp.ClickLogout();
                    lst_status.append("Fail")

            elif act_title != exp_title:
                if self.exp == 'Pass':
                    self.logger.info("**** failed ****")
                    lst_status.append("Fail")
                elif self.exp == 'Fail':
                    self.logger.info("**** passed ****")
                    lst_status.append("Pass")
            print(lst_status)

        if "Fail" not in lst_status:
            self.logger.info("******* DDT Login test passed **********")
            self.driver.close()
            assert True
        else:
            self.logger.error("******* DDT Login test failed **********")
            self.driver.close()
            assert False
        self.logger.info(" **** End of Login DDT test ****")
        self.logger.info(" **** Login DDT is completed ****")