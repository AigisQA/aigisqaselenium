import pytest
from selenium import webdriver
from pageObjects.LoginPage import LoginPage
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen


class Test_001_Login:
    baseURL = ReadConfig.getApplicationURL()
    username = ReadConfig.getUsername()
    password = ReadConfig.getPassword()

    logger = LogGen.loggen()
    @pytest.mark.sanity
    def test_homePageTitle(self, setup):
        self.logger.info("**************************Test_001_Login **************************")
        self.logger.info("************************** Verifying home page title**************************")
        self.driver = setup
        self.driver.get(self.baseURL)
        actTitle = self.driver.title

        if actTitle == 'Aigis':
            assert True
            self.driver.close()
            self.logger.info("**************************  home page title Passed**************************")
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_homePageTitle.png**************************")
            self.driver.close()
            self.logger.info("**************************  home page title Failed**************************")
            assert False

    @pytest.mark.sanity
    @pytest.mark.regression
    def test_Login(self, setup):
        self.logger.info("************************** Verifying Login Test**************************")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.setUserName(self.username)
        self.lp.password(self.password)
        self.lp.ClickLogin()
        actTitle = self.driver.title

        if actTitle == 'Aigis':
            self.logger.info("**************************  Login Test Passed**************************")
            assert True
            self.driver.close()
        else:
            self.logger.info("**************************  Login Test Failed**************************")
            assert False
            self.driver.close()
