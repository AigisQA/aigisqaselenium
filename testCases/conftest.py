from selenium import webdriver
import pytest

@pytest.fixture()


def setup():
    #driver = webdriver.Chrome(executable_path="D:\Dummy\chromedriver_win32\chromedriver.exe")
    driver = webdriver.Chrome()
    return driver


############## Pytest HTML Report ##########
def pytest_configure(config):
    config._metadata['Project Name'] = 'Altus'
    config._metadata['Module Name'] = 'Login'
    config._metadata['Tester'] = 'Vipin'

# It is hook for delete/Modify Environment info to HTML Report
@pytest.mark.optionalhook
def pytest_metadata(metadata):
    metadata.pop("JAVA_HOME", None)
    metadata.pop("Plugins", None)